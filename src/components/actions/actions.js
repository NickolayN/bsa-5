import React from 'react';
import './actions.css';

class Actions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isShown: false
    }

    this.toggle = this.toggle.bind(this);
    this.showActions = this.showActions.bind(this);
  }

  toggle() {
    this.setState({
      isShown: !this.state.isShown
    })
  }

  showActions() {
    return (
      <div className="Options">
        <button onClick={this.props.onEdit}>Edit...</button>
        <button onClick={this.props.onDelete}>Delete...</button>
      </div>
    );
  }

  render() {
    return (
      <div className="Actions"
        onMouseEnter={this.toggle}
        onMouseLeave={this.toggle}
      >
        <button className="ToggleButton">
          &#9776;
        </button>
        {this.state.isShown? this.showActions() : null}
      </div>
    );
  }
}

export default Actions;