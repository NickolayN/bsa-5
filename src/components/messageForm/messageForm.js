import React from 'react';
import Textarea from '../textarea/textarea';

import './messageForm.css';

class MessageForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      message: props.message
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    const message = e.target.value;
    this.setState({
      ...this.state,
      message
    });
  }

  onSubmit(e) {
    e.preventDefault();
    this.props.onSendMessage(this.state.message);
    this.setState({
      ...this.state,
      message: ''
    });
  }

  render() {
    const className = this.props.className ?
      `${this.props.className} messageForm` :
      'messageForm';

    return (
      <form className={className} onSubmit={this.onSubmit}>
        <Textarea
          value={this.state.message}
          onChange={ this.onChange }
        />
        {this.props.cancelable ?
          <button
            type="button"
            onClick={this.props.onCancel}>
            Cancel
          </button> :
        null}
        <button type="submit">Send</button>        
      </form>
    )
  }
}

export default MessageForm;