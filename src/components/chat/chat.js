import React from 'react';
import Header from '../header/header';
import MessagesList from '../messagesList/messagesList';
import MessageForm from '../messageForm/messageForm';
import { getMessages } from "../../services/messageService";
import Loader from '../loader/loader';

import './chat.css';

const currentMember = {
  user: 'Nick',
  avatar: 'https://i.pravatar.cc/300?img=50'
};

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: null
    };

    this.handleSendMessage = this.handleSendMessage.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleLike = this.handleLike.bind(this);

    getMessages().then(messages => {
      messages.sort((a, b) => b.created_at - a.created_at);
      this.setState({messages});
    });
  }

  handleSendMessage(message) {
    const newMessage = {
      ...currentMember,
      message,
      created_at: Date.now(),
      id: this.getId()
    };
    const messages = [...this.state.messages];
    messages.push(newMessage);

    this.setState({
      ...this.state,
      messages
    });
  }

  handleDelete(id) {
    const messages = [...this.state.messages].filter(message => message.id !== id);
    this.setState({
      ...this.state,
      messages
    });
  }

  handleEdit(id, message) {
    const messages = [...this.state.messages];
    const editableMessage = messages.find(message => message.id === id);
    editableMessage.message = message;
    this.setState({
      ...this.state,
      messages
    });
  }

  handleLike(id) {
    const messages = [...this.state.messages];
    const editableMessage = messages.find(message => message.id === id);
    editableMessage.isLiked = !editableMessage.isLiked;
    this.setState({
      ...this.state,
      messages
    });
    console.log(this.state);
  }

  getId() {
    const lastMessage = this.state.messages[this.state.messages.length - 1];
    return lastMessage.id + 1;
  }

  countParticipants(messages) {
    const participants = new Set();
    messages.forEach(message => {
      if (!participants.has(message.user)) {
        
        participants.add(message.user);
      }
    });

    return participants.size;
  }

  render() {
    const messages = this.state.messages;

    if (!this.state.messages) {
      return <Loader />
    } else {
      return (
        <div className="Chat">
          <header>
            <Header
              participants={this.countParticipants(this.state.messages)}
              messages={messages.length}
              lastMessageDate={messages[messages.length - 1]['created_at']}
            />
          </header>
          <MessagesList
            messages={messages}
            currentMember={currentMember.user}
            onDelete={this.handleDelete}
            onEdit={this.handleEdit}
            onLike={this.handleLike}
          />
          <MessageForm
            onSendMessage={this.handleSendMessage}
          />
        </div>
      )
    }
  }
}

export default Chat;