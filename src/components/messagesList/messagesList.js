import React from 'react';
import Message from '../message/message';
import './messageList.css';

class MessagesList extends React.Component {
  componentDidUpdate = () => {
    this.node.scrollTop = this.node.scrollHeight;
  }

  componentDidMount = () => {
    this.node.scrollTop = this.node.scrollHeight;
  }

  getId(id) {
    return id * 10;
  }
  render() {
    const messages = this.props.messages;  
    return (
        <ul className="Messages-list" ref={node => this.node = node}>
          {
            messages.reduce((prev, message) =>
              {
                if (prev.length === 0) {
                  const nextDate = new Date(message['created_at']);
                  prev.push(<li key={this.getId(message.id)}>{nextDate.toDateString()}</li>)
                }
                if(prev.length >= 1) {
                  const prevDateStr = prev[prev.length - 1].props['created_at'];
                  const nextDateStr = message['created_at'];
                  const prevDate = new Date(prevDateStr);
                  const nextDate = new Date(nextDateStr);
                  if (nextDate.getDate() > prevDate.getDate()) {
                    const id = this.getId(message.id)
                    prev.push(<li key={id}>{nextDate.toDateString()}</li>)
                  }
                }
                
                prev.push(
                  <Message
                    key={message.id}
                    {...message}
                    isOwn={message.user === this.props.currentMember}
                    onDelete={(id) => this.props.onDelete(id)}
                    onEdit={(id, message) => this.props.onEdit(id, message)}
                    onLike={this.props.onLike}
                  />
                )
              return prev;
              }, []
            )
          }
        </ul>
    );
  }
}

export default MessagesList;