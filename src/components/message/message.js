import React from 'react';
import Avatar from '../avatar/avatar';
import Actions from "../actions/actions";
import MessageForm from '../messageForm/messageForm';
import MessageContent from '../messageContent/messageContent';

import './message.css';


class Message extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isEditable: false
    };

    this.handleDelete = this.handleDelete.bind(this);
    this.toggle = this.toggle.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleLike = this.handleLike.bind(this);
  }

  handleDelete() {
    this.props.onDelete(this.props.id);
    this.toggle();
  }

  toggle() {
    this.setState({
      isEditable: !this.state.isEditable
    });
  }

  handleEdit(message) {
    this.toggle();
    this.props.onEdit(this.props.id, message);
  }

  handleLike() {
    this.props.onLike(this.props.id);
  }

  render() {
    let className = this.props.isOwn ? "Message currentMember" : "Message";
    className = this.props.isLiked ? className + " Liked" : className;

    return (
      <li className={ className }>
        <Avatar
          avatar={this.props.avatar}
          user={this.props.user}
        />
        {
          !this.state.isEditable ?
            <MessageContent
              {...this.props}
          /> :
            <MessageForm
              message={this.props.message}
              className={'Edit-message-form'}
              cancelable={true}
              onCancel={this.toggle}
              onSendMessage={this.handleEdit}
            />
        }
        {
          this.props.isOwn ?
            <Actions
              onDelete={this.handleDelete}
              onEdit={this.toggle}
            /> :
            <button className="Like-button" onClick={this.handleLike}>Like</button>
          }
      </li>
    )
  }
}

export default Message;